import React from "react";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import Home from "./page/Home";

export default class App extends React.Component{
  render() {
    return(
        <BrowserRouter>
          <Switch>
            <Route
                path="/"
                render={(props) => (
                    <Home {...props} />
                )}
            />
            <Redirect to="/" />
          </Switch>
        </BrowserRouter>
    )
  }
}