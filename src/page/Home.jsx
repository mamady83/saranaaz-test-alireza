import React from "react";
import Ellipse_5 from "./../assets/img/Ellipse 5.png";
import Ellipse_8_2 from "./../assets/img/Ellipse 8_2.png";
import Ellipse_11 from "./../assets/img/Ellipse 11.png";
import Ellipse_13 from "./../assets/img/Ellipse 13.png";
import Ellipse_12 from "./../assets/img/Ellipse 12.png";
import Ellipse_15 from "./../assets/img/Ellipse 15.png";
// import Ellipse_14 from "./../assets/img/Ellipse 14.png";
import Ellipse_10 from "./../assets/img/Ellipse 10.png";
import Ellipse_9_2 from "./../assets/img/Ellipse 9_2.png";
import Ellipse_9 from "./../assets/img/Ellipse 9.png";
import Ellipse_8 from "./../assets/img/Ellipse 8.png";
import Ellipse_4 from "./../assets/img/Ellipse 4.png";
import Ellipse_3 from "./../assets/img/Ellipse 3.png";
import Ellipse_2 from "./../assets/img/Ellipse 2.png";
import Ellipse_6 from "./../assets/img/Ellipse 6.png";
import Ellipse_7 from "./../assets/img/Ellipse 7.png";
import Ellipse_1 from "./../assets/img/Ellipse 1.png";
// SLIDER IMAGES
import image_1 from "./../assets/img/slider/image 1.png";
// WOMAN
import woman from "./../assets/img/IMG_1253 1.png";

export default class Home extends React.Component {
  render() {
    return(
        <div className="w-100 main-home-content position-relative">

          {/* START BLUR ELLIPSE */}
          <section id="blur_ellipse">
            <div className="position-absolute vw-vh-100">
              <div className="ellipse-14"></div>
            </div>
            <div className="group-118">
              <img className="position-absolute ellipse-5" src={Ellipse_5} alt=""/>
              <img className="position-absolute ellipse-11" src={Ellipse_11} alt=""/>
              <img className="position-absolute ellipse-12" src={Ellipse_12} alt=""/>
              {/*<div className="position-absolute ellipse-14">*/}
              {/*</div>*/}
              {/*<img className="position-absolute ellipse-14" src={Ellipse_14} alt=""/>*/}
              <img className="position-absolute ellipse-15" src={Ellipse_15} alt=""/>
              <img className="position-absolute ellipse-13" src={Ellipse_13} alt=""/>
              <img className="position-absolute ellipse-8_2" src={Ellipse_8_2} alt=""/>
              <img className="position-absolute ellipse-10" src={Ellipse_10} alt=""/>
              <img className="position-absolute ellipse-9_2" src={Ellipse_9_2} alt=""/>
            </div>
            <div className="group-2">
              <img src={Ellipse_9} className="position-absolute ellipse-9" alt=""/>
            </div>
            <div className="group-1">
              <img className="position-absolute"
                   style={{
                     width: "177px",
                     height: "177px",
                     left: "671px",
                     top: "618px",
                   }}
                   src={Ellipse_8} alt=""/>
              <img className="position-absolute"
                   style={{
                     left: "597px",
                     top: "72px",
                     opacity: "0.5"
                   }}
                   src={Ellipse_4}
                   alt=""
              />
              <img className="position-absolute"
                   style={{
                     width: "307px",
                     height: "307px",
                     left: "277px",
                     top: "494px"
                   }}
                   src={Ellipse_3} alt=""/>
              <img className="position-absolute"
                   src={Ellipse_2} alt=""
                   style={{
                     width: "307px",
                     height: "307px",
                     left: "292px",
                     top: "215px"
                   }}
              />
              <img className="position-absolute ellipse-7" src={Ellipse_7} alt=""/>
              <img className="position-absolute ellipse-6" src={Ellipse_6} alt=""/>
              <img className="position-absolute ellipse-1" src={Ellipse_1} alt=""/>
            </div>
          </section>
          {/* END BLUR ELLIPSE */}

          <div className="position-relative main-content-space">
            <div className="home-bg">
              <div className="position-absolute" style={{
                top: "159px",
                left: "-199px"
              }}
              >
                <div className="position-relative" id="saranaz-text-box">
                  <h2 className="h1-font-sizes w-max hi-c">Hi! I’m</h2>
                  <div className="mb-0 position-absolute" style={{
                    top: "83px"
                  }}>
                    <h1 className="saranaaz-text">Saranaaz</h1>
                    <div className="position-absolute">
                      <p className="p-fontSize SFProDisplay-Light light-font-c">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the , when an unknown printer took a galley of type and scrambled
                      </p>
                      {/* ABOUT US */}
                      <section className="d-flex pt-2">
                        <h6 className="mb-0 SFProDisplay-Light light-font-c h-fit pt-3">About Me</h6>
                        <span className="pl-4 h-fit pt-3">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" clipRule="evenodd"
                            d="M20 10C20 4.47768 15.5223 0 10 0C4.47768 0 0 4.47768 0 10C0 14.9911 3.65625 19.1281 8.4375 19.879V12.8915H5.89777V10H8.4375V7.79688C8.4375 5.29107 9.9308 3.9058 12.2147 3.9058C13.3089 3.9058 14.4536 4.10134 14.4536 4.10134V6.5625H13.192C11.9504 6.5625 11.5621 7.33304 11.5621 8.125V10H14.3353L13.8924 12.8915H11.5625V19.8799C16.3438 19.1295 20 14.9924 20 10Z"
                            fill="#A5B6C1"/>
                      </svg>
                    </span>
                        <span className="pl-4 h-fit pt-3">
                      <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                          d="M19.8715 3.31253C19.8715 1.55449 18.5784 0.140252 16.9805 0.140252C14.8162 0.0390673 12.6089 0 10.3531 0H9.64989C7.39961 0 5.18839 0.0390674 3.02406 0.140643C1.43011 0.140643 0.136974 1.5627 0.136974 3.32073C0.039305 4.71114 -0.00210647 6.10194 0.000237572 7.49274C-0.00366917 8.88354 0.0406073 10.2756 0.133067 11.6691C0.133067 13.4271 1.4262 14.853 3.02015 14.853C5.29387 14.9585 7.6262 15.0054 9.99759 15.0015C12.3729 15.0093 14.6987 14.9598 16.975 14.853C18.5729 14.853 19.866 13.4271 19.866 11.6691C19.9598 10.2743 20.0028 8.88354 19.9989 7.48884C20.0077 6.09803 19.9653 4.70593 19.8715 3.31253ZM8.0872 11.3253V3.64851L13.752 7.48493L8.0872 11.3253Z"
                          fill="#A5B6C1"
                      />
                      </svg>
                    </span>
                        <span className="pl-4 h-fit pt-3">
                      <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M20 1.89584C19.2498 2.22182 18.456 2.43658 17.6438 2.53334C18.4974 2.0335 19.1393 1.23941 19.4492 0.300007C18.6421 0.77192 17.7606 1.10304 16.8425 1.27917C16.4559 0.873978 15.9909 0.551634 15.4758 0.331734C14.9607 0.111834 14.4063 -0.00102805 13.8463 7.05601e-06C11.5788 7.05601e-06 9.74375 1.80834 9.74375 4.03751C9.74214 4.34758 9.77767 4.65672 9.84958 4.95834C8.22362 4.88212 6.63147 4.46733 5.17499 3.74053C3.71852 3.01373 2.42979 1.99091 1.39125 0.737507C1.02691 1.35172 0.834222 2.05253 0.833333 2.76667C0.833333 4.16667 1.56375 5.40417 2.66667 6.12917C2.01322 6.11367 1.37316 5.94072 0.800833 5.62501V5.67501C0.800833 7.63334 2.2175 9.26251 4.0925 9.63334C3.73991 9.72733 3.37657 9.77496 3.01167 9.77501C2.75274 9.77546 2.4944 9.75034 2.24042 9.70001C2.76167 11.3042 4.27875 12.4708 6.07583 12.5042C4.61557 13.6296 2.82276 14.2378 0.979167 14.2333C0.651924 14.2329 0.324986 14.2134 0 14.175C1.87549 15.3726 4.0556 16.0061 6.28083 16C13.8375 16 17.9658 9.84584 17.9658 4.50834C17.9658 4.33334 17.9613 4.15834 17.9529 3.98751C18.7542 3.41753 19.4474 2.70922 20 1.89584Z" fill="#A5B6C1"/>
                      </svg>
                    </span>
                        <span className="pl-4 h-fit pt-3">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M14.1665 1.66652C15.2707 1.66982 16.3286 2.10989 17.1094 2.89064C17.8901 3.67138 18.3302 4.72935 18.3335 5.83348V14.1665C18.3302 15.2707 17.8901 16.3286 17.1094 17.1094C16.3286 17.8901 15.2707 18.3302 14.1665 18.3335H5.83348C4.72935 18.3302 3.67138 17.8901 2.89064 17.1094C2.10989 16.3286 1.66982 15.2707 1.66652 14.1665V5.83348C1.66982 4.72935 2.10989 3.67138 2.89064 2.89064C3.67138 2.10989 4.72935 1.66982 5.83348 1.66652H14.1665ZM14.1665 0H5.83348C2.625 0 0 2.625 0 5.83348V14.1665C0 17.375 2.625 20 5.83348 20H14.1665C17.375 20 20 17.375 20 14.1665V5.83348C20 2.625 17.375 0 14.1665 0Z"
                            fill="#A5B6C1"/>
                        <path
                            d="M15.4165 5.83362C15.1693 5.83362 14.9276 5.76031 14.7221 5.62296C14.5165 5.4856 14.3563 5.29038 14.2617 5.06197C14.1671 4.83356 14.1423 4.58223 14.1905 4.33976C14.2388 4.09728 14.3578 3.87455 14.5326 3.69974C14.7075 3.52492 14.9302 3.40587 15.1727 3.35764C15.4151 3.30941 15.6665 3.33416 15.8949 3.42877C16.1233 3.52338 16.3185 3.68359 16.4559 3.88916C16.5932 4.09472 16.6665 4.33639 16.6665 4.58362C16.6669 4.74787 16.6348 4.91057 16.5721 5.06239C16.5094 5.21421 16.4173 5.35215 16.3012 5.46829C16.185 5.58443 16.0471 5.67649 15.8953 5.73919C15.7435 5.80188 15.5808 5.83397 15.4165 5.83362ZM10 6.66665C10.6593 6.66665 11.3038 6.86216 11.852 7.22845C12.4002 7.59474 12.8274 8.11535 13.0797 8.72447C13.332 9.33358 13.3981 10.0038 13.2694 10.6505C13.1408 11.2971 12.8233 11.8911 12.3571 12.3573C11.8909 12.8235 11.297 13.1409 10.6503 13.2696C10.0037 13.3982 9.33345 13.3322 8.72433 13.0799C8.11522 12.8276 7.5946 12.4003 7.22831 11.8521C6.86203 11.3039 6.66652 10.6594 6.66652 10.0001C6.66747 9.11633 7.01897 8.269 7.64392 7.64405C8.26886 7.01911 9.1162 6.6676 10 6.66665ZM10 5.00014C9.0111 5.00014 8.0444 5.29338 7.22215 5.84279C6.39991 6.39219 5.75904 7.17309 5.3806 8.08672C5.00217 9.00035 4.90315 10.0057 5.09608 10.9756C5.289 11.9455 5.76521 12.8364 6.46447 13.5357C7.16373 14.2349 8.05465 14.7111 9.02455 14.9041C9.99446 15.097 10.9998 14.998 11.9134 14.6195C12.827 14.2411 13.6079 13.6002 14.1574 12.778C14.7068 11.9557 15 10.989 15 10.0001C15 8.67405 14.4732 7.40228 13.5355 6.4646C12.5979 5.52692 11.3261 5.00014 10 5.00014Z"
                            fill="#A5B6C1"/>
                        </svg>
                    </span>
                        <div className="pl-4 h-fit">
                          <button className="d-flex contact-btn-bg border-0 pt-3 px-3 pb-2 radius-10">
                        <span className="h-fit">
                          <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                               xmlns="http://www.w3.org/2000/svg">
                          <path
                              d="M10.481 18.71L21.468 12.878C22.177 12.502 22.177 11.49 21.468 11.113L10.497 5.29C9.669 4.85 8.741 5.68 9.092 6.546L11.281 11.946L9.074 17.456C8.728 18.323 9.655 19.148 10.481 18.71V18.71Z"
                              stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
                          <path d="M11.28 11.95L22 12" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
                          <path d="M4 15H5.6" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
                          <path d="M3 12H5.6" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
                          <path d="M2 9H5.6" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
                          </svg>
                        </span>
                            <h6 className="mb-0 SFProDisplay-Light contact-btn-c c-w p-fontSize pt-1 pl-2 pr-2">Contact</h6>
                          </button>
                        </div>
                      </section>
                    </div>
                  </div>

                </div>
              </div>
              <section className="row">
                <div className="col-md-6 saranaaz-image-space">
                  <img
                      className="position-relative"
                      id="woman"
                      src={woman} alt=""
                      style={{
                        zIndex: "-1"
                      }}
                  />
                </div>
                {/* START Portfolio SECTION */}
                <div className="portfollo-width w-100 col-md-6">
                  <h1 className="SFProDisplay-Thin c-w">Portfolio</h1>
                  <div className="c-w mt-4 d-flex slider-gap row">
                    <article>
                      <div>
                        <img className="radius-10" src={image_1} alt=""/>
                      </div>
                      <h6 className="mb-0 SFProDisplay-Light pt-3 pl-2">Bache Mohandes</h6>
                    </article>
                    <article>
                      <div>
                        <img className="radius-10" src={image_1} alt=""/>
                      </div>
                      <h6 className="mb-0 SFProDisplay-Light pt-3 pl-2">Bache Mohandes</h6>
                    </article>
                  </div>
                </div>
                {/* END Portfolio SECTION */}
              </section>

            </div>
          </div>
        </div>
    )
  }
}