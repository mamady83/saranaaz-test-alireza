window.addEventListener('scroll', () => {
  console.log('scrolling...')
})
//
// $(window).scroll(function(event){
//   console.log('scroll')
//   var st = $(this).scrollTop();
//   if (st > lastScrollTop){
//     // downscroll code
//   } else {
//     // upscroll code
//   }
//   lastScrollTop = st;
// });
$(window).bind('mousewheel', function(event) {
  if (event.originalEvent.wheelDelta >= 0) {
    show_woman()
    show_text()
    console.log('Scroll up');
  }
  else {
    fade_woman()
    fade_text()
    console.log('Scroll down');
  }
});

function fade_woman() {
  $("#blur_ellipse")
      .css("transition", "ease-out 0.25s")
      .css("opacity", "0")
  $(".home-bg")
      .css('left', '-200px')
      .css("transition", "ease-out 1s")
  $("#woman")
      .css('transform', 'scale(0.75)')
      .css("transition", "ease-out 1s")
}

function show_woman() {
  $("#blur_ellipse")
      .css("transition", "ease-out 1s")
      .css("opacity", "1")
  $(".home-bg")
      .css('left', '499px')
      .css("transition", "ease-out 1s")
  $("#woman")
      .css('transform', 'scale(1)')
      .css("transition", "ease-out 1s")
}

function fade_text() {
  // $("#saranaz-text-box").animate({'opacity':'0'},500);
  // var opacity = $("#saranaz-text-box").css("opacity")
  // var new_opacity = opacity / 2
  // if (new_opacity == 0.25) new_opacity = 0
  // new_opacity = Math.round(new_opacity * 250) / 250
  // console.log(opacity)
  // console.log(new_opacity)
  $("#saranaz-text-box")
      .css("opacity", 0)
      .css("transition", "ease-out 0.25s")
}

function show_text() {
  // $("#saranaz-text-box").animate({'opacity':'1'},500);
  // var opacity = $("#saranaz-text-box").css("opacity")
  // if (opacity == 0) opacity = 0.25
  // if (opacity == 1) return
  // var new_opacity = opacity * 2
  // new_opacity = Math.round(new_opacity * 250) / 250
  // console.log(new_opacity)
  $("#saranaz-text-box")
      .css("opacity", 1)
      .css("transition", "ease-in 0.5s")
  // console.log(new_opacity)
}